# IP Ranges für AustroEdit

Die Daten sind aus unterschiedlichen Quellen gesammelt und erheben keinen Anspruch auf Vollständigkeit oder Richtigkeit!

Der Großteil der Daten kommt aus dem BGP Routingtable oder aus RDNS Informationen.

[Bin ich in der Liste? Und mehr Infos.](https://ranges.vikoe.eu/)

[Bot der mit diesen Daten Wikipedia Edits postet](https://botsin.space/@austroedit)

## Dateienbeschreibung

| Datei                        | Was?                                                         |
|------------------------------|--------------------------------------------------------------|
| anwaelte.json                | Anwälte, jo eh                                               |
| ausgelagert.json             | Dinge, die zumindest teilweise der öffentlichen Hand gehören |
| banken_versicherungen.json   | Banken und Versicherungen                                    |
| bildung.json                 | Schulen usw.                                                 |
| botschaften.json             | Botschaften in Österreich, österreichische Botschaften       |
| bund.json                    | Unmittelbare Bundesverwaltung, Gerichte                      |
| gemeinden.json               | Gemeinden eben                                               |
| gesundheit.json              | Vor allem Krankenhäuser                                      |
| interessensvertretungen.json | Gewerkschaften, Parteien, Interessensvertretungen            |
| kirchen.json                 | Religionsgemeinschaften und was dazu gehört                 |
| koerperschaften.json         | Kammern und internationale Organisationen in Österreich      |
| laender.json                 | Landesregierungen und deren Ämter                            |
| magistrate.json              | Stadtmagistrate                                              |
| medien.json                  | Medienunternehmen                                            |
| privat.json                  | Sonstige große oder sonst interessante Sachen                |
| schulen.json                 | Schulen                                                      |
| sv.json                      | Sozialversicherungsträger und Krankenfürsorgeanstalten       |
| unis.json                    | Universitäten und Fachhochschulen                            |
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <title>AustroEdit Ranges</title>
    <link rel="stylesheet" href="chota.css">
</head>
<body>
<?php
require __DIR__ . "/vendor/autoload.php";

if (filter_var($_SERVER["REMOTE_ADDR"], FILTER_VALIDATE_IP)) {
    $user_ip = \IPLib\Factory::parseAddressString($_SERVER["REMOTE_ADDR"]);
} else {
    die("Malformed IP");
}

$ip_list = json_decode(
    file_get_contents("all.json"),
    true
);

$name = false;
foreach ($ip_list as $inst) {
    foreach ($inst["ranges"] as $loop_subnet) {
        $loop_subnet = \IPLib\Factory::parseRangeString($loop_subnet);

        if ($user_ip->matches($loop_subnet)) {
            $name = $inst["name"];
            $subnet = $loop_subnet->toString();
            break;
        }
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col text-center">
            <h1>
                AustroEdit Ranges
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <header>
                    <h2 class="text-center">
                        ???
                    </h2>
                </header>
                <p>
                    Ich bin manuell Dumps aus den Routingtabellen durchgegangen und habe IPs staatlicher (und verwandter) österreichischer Institutionen gesammelt. Trotz aller Sorgfalt kann es sein, dass die Einträge nicht 100% korrekt sind.
                    Ich übernehme keine wie auch immer ausgestaltete Haftung für die Richtigkeit der Daten.<br>
                    Die Datei "privat_interessant.json" enthält Firmen, Vereine und ähnliches, die nicht mit staatlichen Stellen in Verbindung stehen, aber auch interessant sein können.
                </p>
                <footer>
                    <a class="button outline dark" href="all.json">
                        Download
                    </a>
                    <a class="button outline dark" href="https://gitlab.com/deadda7a/austroedit-ranges">
                        Repo
                    </a>
                </footer>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <header>
                    <h2 class="text-center">
                        Lizenz
                    </h2>
                </header>
                <p>
                    <a href="https://opendatacommons.org/licenses/odbl/1-0/">
                        Open Data Commons Open Database License v1.0
                    </a>
                </p>
                <h3>
                    Ist meine IP in der Datenbank?
                </h3>
                <h4 class="text-center">
                    Deine IP lautet<br>
                    <?php echo($user_ip->toString()); ?><br>
                    ... und sie ist<br>
                    <span class="bd-<?php echo ($name) ? "success" : "error"; ?>">
                        <?php echo ($name) ? sprintf("in der Datenbank</span><br> unter dem Eintrag<br>&raquo;%s&#xab;<br>im Subnetz<br>%s!", $name, $subnet) : "nicht in der Datenbank!"; ?>
                </h4>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <header>
                    <h2 class="text-center">FAQ</h2>
                </header>
                <ul>
                    <li>
                        <i>Ich möchte nicht auf der Liste sein. Wie kann ich mich löschen lassen?</i><br>
                        Gar nicht. Die Datengrundlage dieser Liste ist öffentlich. Wenn du nicht mehr auf der Liste sein
                        willst, musst du dich an deinen Internetprovider wenden, dass die Beschreibung des Subnetzes
                        angepasst wird.
                    </li>
                    <li>
                        <i>Mein Eintrag ist falsch!</i><br>
                        Auch hier: Wende dich an deinen Internetprovider. <a href="https://blog.sebastian-elisa-pfeifer.eu/contact/">Schreib mir</a> aber auch, dann passe ich es an.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col text-left">
            <h2>
                Liste der enthaltenen Institutionen...
            </h2>
            <table class="striped sortable asc">
                <thead>
                    <tr>
                        <th id="name">
                            Name
                        </th>
                        <th>
                            IPv4 Adressen
                        </th>
                        <th>
                            IPv6 Adressen
                        </th>
                        <th class="no-sort">
                            Wert der IPv4 Adressen*
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($ip_list as $inst) {
                    ?>
                    <tr>
                        <td>
                            <?php echo ($inst["name"]); ?>
                        </td>
                    <?php
                            $ip4size = (float) 0;
                            $ip6size = (float) 0;

                            foreach ($inst["ranges"] as $range) {
                                $range = \IPLib\Factory::parseRangeString($range);
                                if ($range->getAddressType() == 4) {
                                    $ip4size = $ip4size + $range->getSize();
                                } else {
                                    $ip6size = $ip6size + $range->getSize();
                                }
                            }
                    ?>
                        <td data-sort="<?php echo ($ip4size != 0) ? $ip4size : "0"; ?>">
                            <?php
                            if ($ip4size != 0) {
                                echo(number_format($ip4size, 0, ",", "."));
                            } else {
                                echo "0";
                            }
                            ?>
                        </td>
                        <td data-sort="<?php echo ($ip6size != 0) ? round($ip6size) : "0"; ?>">
                            <?php
                            // OK this is the most hacky thing but whatever
                            if ($ip6size != 0) {
                                $number_as_string = (string) sprintf("%e", $ip6size);
                                if (!str_contains($number_as_string, "e+0")) {
                                    $ip6size_parts = explode("e+", $number_as_string);
                                    printf("%01.2f&times;10<sup>%d</sup> ", $ip6size_parts[0], $ip6size_parts[1]);
                                } else {
                                    echo $ip6size;
                                }
                            } else {
                                echo "0";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($ip4size >= 256) {
                                $prize = $ip4size * 45;
                                $formatter = numfmt_create("de_AT", NumberFormatter::CURRENCY);
                                $formatter->setTextAttribute(NumberFormatter::CURRENCY_CODE, "EUR");
                                $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);

                                echo(numfmt_format_currency($formatter, $prize, "EUR"));
                            } else {
                                echo "-";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <p>
                *Bei einem Preis von 45€ (netto) pro IP Adresse (RIPE Region, 2022).<br>
                Das impliziert aber dass diese IPs wirklich den Stellen gehören, und nicht nur von ihnen genutzt werden.<br>
                Dazu muss es mindestens ein /24 sein, also 256 Adressen.
            </p>
            <p>
                Revision ##COMMIT_SHORT_SHA##
            </p>
        </div>
    </div>
</div>
<link rel="stylesheet" href="sortable-base.min.css">
<script src="sortable.min.js"></script>
<script>
window.addEventListener("load", function () {
    const el = document.getElementById("name")
        if (el) {
          el.click()
        }
    })
</script>
</body>
</html>
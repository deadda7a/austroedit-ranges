#!/usr/bin/env python3

import os, io, csv, sys, ipaddress
from ipwhois import IPWhois
work_dir = os.getcwd()
ranges = []

def do_whois(ip):
    try:
        obj = IPWhois(ip)
        whois_data = obj.lookup_rdap(depth=0)
    except Exception:
        return None

    try:
        desc = whois_data['network']['remarks'][0]['description']
    except Exception:
        try:
            desc = whois_data['network']['name']
        except Exception:
            return None

    if not '\n' in desc:
        return desc

    try:
        desc_split = desc.split('\n')
        for _entry in desc_split:
            if _entry:
                return _entry
    except Exception:
        return None

file_name = input("Datei: ").strip()

input_file = io.open(
    os.path.join(
        work_dir,
        f"{file_name}.csv"
    ),
    mode="r",
    encoding="utf-8"
)

csv_data = csv.reader(
    io.open(
        os.path.join(
            work_dir,
            f"{file_name}.csv"
        ),
        mode="r",
        encoding="utf-8"
    ),
    delimiter=";"
)

next(csv_data, None)

for line in csv_data:
    ip_range = line[0].strip()
    name_in_file = line[1].strip()

    ip = ipaddress.ip_network(ip_range).network_address

    if name_in_file:
        print(f"{ip_range} -> {name_in_file}")
    else:
        desc = do_whois(ip)
        if desc:
            print(f"{ip_range} -> {desc}")
        else:
            print(f"{ip_range} -> LEER")

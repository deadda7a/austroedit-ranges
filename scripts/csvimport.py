#!/usr/bin/env python3

import os, csv, json, io
from netaddr import cidr_merge

file_name = input("Datei: ").strip()

work_dir = os.getcwd()
datensaetze = []

csv_data = csv.reader(
    io.open(
        os.path.join(
            work_dir,
            f"{file_name}.csv"
        ),
        mode="r",
        encoding="utf-8"
    ),
    delimiter=";"
)

next(csv_data, None)

for line in csv_data:
    ip_range = line[0].strip()
    name = line[1].strip()
    phrase = line[2].strip()

    print(ip_range, name)

    if name not in [d["name"] for d in datensaetze]:
        datensaetze.append(
            {
                "phrase": phrase,
                "name": name,
                "ranges": [
                    ip_range
                ]
            }
        )
    else:
        for datensatz in datensaetze:
            if name == datensatz["name"]:
                datensatz["ranges"].append(ip_range)
                all_ranges = cidr_merge(datensatz["ranges"])
                datensatz["ranges"] = [str(single_range.cidr) for single_range in all_ranges]

out_file = io.open(
    os.path.join(
        work_dir,
        "ranges",
        f"{file_name}.json"
    ),
    mode="w",
    encoding="utf-8"
)

out_file.write(
    json.dumps(
        datensaetze,
        indent=2,
        ensure_ascii=False
    )
)

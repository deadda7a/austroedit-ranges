#!/usr/bin/env python3

from netaddr import cidr_merge
import io, os, json

ranges = []

name = input("Name: ").strip()
phrase = input("Phrase: ").strip()

while "end" not in ranges:
    ranges.append(input("Range (end to exit): ").strip())

ranges.remove("end")

print(
    json.dumps(
        {
            "phrase": phrase,
            "name": name,
            "ranges": [str(single_range.cidr) for single_range in cidr_merge(ranges)]
        },
        indent=2,
        ensure_ascii=False
    )
)
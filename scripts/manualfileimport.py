#!/usr/bin/env python3

from netaddr import cidr_merge
import io, os, json

work_dir = os.getcwd()
ranges = []

file_name = input("Datei: ").strip()
name = input("Name: ").strip()
phrase = input("Phrase: ").strip()

input_file = io.open(
    os.path.join(
        work_dir,
        f"{file_name}.txt"
    ),
    mode="r",
    encoding="utf-8"
)

for line in input_file:
    ip_range = line.strip()
    ranges.append(ip_range)

print(
    json.dumps(
        {
            "phrase": phrase,
            "name": name,
            "ranges": [str(single_range.cidr) for single_range in cidr_merge(ranges)]
        },
        indent=2,
        ensure_ascii=False
    )
)
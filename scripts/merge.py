#!/usr/bin/env python3

import os, json


def flatten(l):
    return [item for sublist in l for item in sublist]


work_dir = os.getcwd()
ranges_path = os.path.join(work_dir, "ranges")

out_list = []

for json_file in [x for x in os.listdir(ranges_path) if x.endswith(".json")]:
    if json_file == "all.json":
        continue
    print(json_file)
    with open(os.path.join(ranges_path, json_file), "rb") as infile_handler:
        out_list.append(json.load(infile_handler))

with open(os.path.join(work_dir, "all.json"), "w") as outfile_handler:
    json.dump(flatten(out_list), outfile_handler, ensure_ascii=False)

#!/usr/bin/env python3

import os, sys, json, ipaddress
from tqdm import tqdm
from loguru import logger as log

log.remove()
log.add(
    lambda msg: tqdm.write(msg, end=""),
    format="<green>{time:YYYY-MM-DD}</green> <yellow>{time:HH:mm:ss.SSS ZZ}</yellow> <level>{level}</level>: {message}",
    colorize=True,
    level="INFO"
)

work_dir = os.getcwd()
ranges_path = os.path.join(work_dir, "ranges")
invalid_name_list = []
all_ranges = {}
error_list = []
all_names = []
total_ranges_count = 0
json_files = [x for x in os.listdir(ranges_path) if x.endswith(".json")]

for json_file in json_files:
    with open(os.path.join(ranges_path, json_file), "rb") as infile_handler:
        try:
            for entry in json.load(infile_handler):
                ranges_count = len(entry["ranges"])
                total_ranges_count = total_ranges_count+ranges_count
        except json.decoder.JSONDecodeError:
            log.critical(f"File {json_file} is invalid!")
            sys.exit(1)

with tqdm(total=total_ranges_count, position=0, desc="Total", colour="red") as pbar_total:
    for json_file in json_files:
        names_in_file = []

        with open(os.path.join(ranges_path, json_file), "rb") as infile_handler:
            file_data = json.load(infile_handler)

            with tqdm(total=len(file_data), position=1, desc=f"File: {json_file}", leave=None, colour="green") as pbar_file:
                for entry in file_data:
                    name = entry["name"]
                    with tqdm(total=len(entry["ranges"]), position=2, desc=f"Entry: {name}", leave=None, colour="blue") as pbar_entry:
                        if name in names_in_file:
                            error_list.append(f"Duplicate name {name} in {json_file}")
                        else:
                            names_in_file.append(name)

                        if name in all_names:
                            error_list.append(f"Global duplicate name {name}")
                        else:
                            all_names.append(name)

                        for _range in entry["ranges"]:
                            to_add = True

                            try:
                                _net = ipaddress.ip_network(_range)
                            except ValueError:
                                error_list.append(f"Invalid range {_range}")
                                continue

                            for _outer_range, desc in all_ranges.items():
                                if _net.overlaps(_outer_range):
                                    error_list.append(f"Duplicate Range for {name}: {_range} seen in {json_file}, also seen in {_outer_range} for {desc['name']} in {desc['file']}")
                                    to_add = False

                            if to_add:
                                all_ranges[_net] =  {
                                    'name': name,
                                    'file': json_file
                                }

                            pbar_total.update(1)
                            pbar_entry.update(1)

                    pbar_file.update(1)

if error_list:
    log.critical("Check failed!")
    [log.error(invalid) for invalid in error_list]
    exit(1)
else:
    log.success("Check ok!")
    exit(0)

#!/usr/bin/env python3

import os, json, csv, io

work_dir = os.getcwd()
ranges_path = os.path.join(work_dir, "ranges")
file_name = input("Datei: ").strip()
range_list = []

with io.open(os.path.join(ranges_path, f"{file_name}.json"), mode="r", encoding="utf-8") as in_file_handler:
    for entry in json.load(in_file_handler):
        for ip_range in entry["ranges"]:
            print(ip_range, entry["name"])
            range_list.append(
                {
                    "ip_range": ip_range,
                    "name": entry["name"],
                    "phrase": entry["phrase"]
                }
            )

with io.open(os.path.join(work_dir, f"{file_name}.csv"), mode="w", encoding="utf-8") as export_file_handler:
    writer = csv.DictWriter(
        f=export_file_handler,
        fieldnames=range_list[0].keys(),
        dialect="excel",
        delimiter=";"
    )
    writer.writeheader()
    writer.writerows(range_list)
